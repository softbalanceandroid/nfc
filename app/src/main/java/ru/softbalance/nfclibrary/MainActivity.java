package ru.softbalance.nfclibrary;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import ru.softbalance.nfc.NFCBinder;

/*
 *    This sample shows as how to process NFC signals in Activity
 *
 *    NFC use ru.softbalance.nfcibeacon.NFCBinder with all the static methods,
 *    		follow instruction in NFCBinder
 *
*/
public class MainActivity extends AppCompatActivity implements NFCBinder.onNfcTagDetected {

    Handler executor = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Let's check if nfcAdapter available on our device
        switch (NFCBinder.nfcAdapterState(this)) {
            case NFCBinder.ADAPTER_DISABLED:
                showEnableNcfDialog();
                break;
            case NFCBinder.ADAPTER_NOT_FOUND:
                Toast.makeText(getApplicationContext(), "Bad news! NFC chip not found, and tag never to be detected at this device",
                        Toast.LENGTH_LONG).show();
                break;
            case NFCBinder.ADAPTER_READY:
                Toast.makeText(getApplicationContext(), "Ready to detect NFC tag",
                        Toast.LENGTH_LONG).show();
                break;
            default:
                break;
        }
    }

    //******************** All the NFC-related things are here >>>>> ********************//
    @Override
    public void onResume() {
        super.onResume();
        NFCBinder.enableNFCForegroundMode(this);
    }

    @Override
    // Here's the place to process detected NFC tag, if NFCBinder.enableNFCForegroundMode() was done in onResume
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        // Let's check if there's really NFC-description Intent
        if (NFCBinder.isNfcIntent(intent)){
            // now, get Tag Id from Intent
            String idDec = NFCBinder.getDecTagId(intent, true);
            String idHex = NFCBinder.getHexTagId(intent);
            Toast.makeText(getApplicationContext(), "Detected NFC tag " + idHex + " -> " + idDec, Toast.LENGTH_SHORT).show();
            TextView tl  = (TextView) findViewById(R.id.textLog);
            tl.append(getCurrentTimeFormat("HH:mm:ss")  + " - NFC tag id " + idHex + " -> " + idDec + " found\n");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        NFCBinder.disableNFCForegroundMode(this);
    }
    //******************** <<<<< All the NFC-related things are here ********************//


    //********************  Some more ancillary (UI-related) things are here ********************//
    private void showEnableNcfDialog(){
        new AlertDialog.Builder(this)
                .setMessage("NFC disabled!")
                .setPositiveButton("Please Enable NFC",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface arg0,
                                                int arg1) {
                                Intent setnfc = new Intent(
                                        Settings.ACTION_WIRELESS_SETTINGS);
                                startActivity(setnfc);
                            }
                        })
                .setOnCancelListener(
                        new DialogInterface.OnCancelListener() {
                            public void onCancel(DialogInterface dialog) {
                                finish(); // exit application if user
                            }
                        }).create().show();
    }

    @SuppressLint("SimpleDateFormat")
    private String getCurrentTimeFormat(String timeFormat){
        String time = "";
        SimpleDateFormat df = new SimpleDateFormat(timeFormat);
        Calendar c = Calendar.getInstance();
        time = df.format(c.getTime());

        return time;
    }
}

