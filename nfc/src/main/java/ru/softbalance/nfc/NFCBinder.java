package ru.softbalance.nfc;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;

/*
 * Here are some functions to detect NFC tags in foreground mode of your activity, and get it's ID.
 * Based at https://developer.android.com/guide/topics/connectivity/nfc/advanced-nfc.html.
 * 
 * You should override 
 * 		onResume - enable dispatch: NFCBinder.enableNFCForegroundMode(this),
 * 		onPause - disable dispatch: NFCBinder.disableNFCForegroundMode(this) and 
 * 		onNewIntent - tag processing: NFCBinder.isNfcIntent(intent) + NFCBinder.getTagId(intent)
 * in your activity, as it done in SampleNFCActivity.
 * 
 * You are welcome to use interface onNfcTagDetected in order to remind you what to override.
 * Also you can check NFCBinder.nfcAdapterState(this).
 * 
 * Don't forget to set android.permission.NFC in uses-permission!
 * 
 * For any problem contact stetsenkom@gmail.com. Have a nice day! :-)
 * (c) Mike Stetsenko, 2014
*/
public class NFCBinder {
	
	// thats the way to translate byte array to string, from O'Reilly's "Programming Android" samples
	// https://github.com/bmeike/ProgrammingAndroidExamples/blob/master/SensorDemos/src/com/oreilly/demo/android/pa/sensordemo/NFC.java
	private final static char[] HEX = new char[]{ '0', '1', '2', '3', '4', '5', '6', '7','8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };
	
	public static final String ADAPTER_NOT_FOUND = "nfc_adapter_not_found";
	public static final String ADAPTER_DISABLED  = "nfc_adapter_disabled";
	public static final String ADAPTER_READY     = "nfc_adapter_ready";
	
	public interface onNfcTagDetected {
		public void onPause();
		public void onResume();
		public void onNewIntent(Intent intent);
	}
	
	// NDEF Fields
	private static NfcAdapter mNfcAdapter;
	private static IntentFilter[] mNdefExchangeFilters;
	private static PendingIntent mNfcPendingIntent;
	
	public static boolean isNfcIntent(Intent intent) {
		if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
			return true;
		} else {
			return false;
		}
	}
	
	public static void enableNFCForegroundMode(Activity activity) {
		if (activity != null && nfcAdapterState(activity) == ADAPTER_READY){
			// Activate the NFC Adapter
			mNfcAdapter = NfcAdapter.getDefaultAdapter(activity);
	
			// Create the Intent
			mNfcPendingIntent = PendingIntent.getActivity(activity, 0, new Intent(activity,
													activity.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
	
			// foreground mode gives the current active application priority for
			// reading scanned tags
			mNdefExchangeFilters = new IntentFilter[] { new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED) };
	
			mNfcAdapter.enableForegroundDispatch(activity, mNfcPendingIntent,
					mNdefExchangeFilters, null);
		}
	}
	
	public static void disableNFCForegroundMode(Activity activity) {
		if (activity != null && nfcAdapterState(activity) == ADAPTER_READY){
			mNfcAdapter.disableForegroundDispatch(activity);
		}
	}
	
	/**
	 * Method which checks for the existence of an NFC Adapter
	 */
	public static String nfcAdapterState(Activity activity) {
		if (activity != null && NfcAdapter.getDefaultAdapter(activity) != null) {
			if (NfcAdapter.getDefaultAdapter(activity).isEnabled()) {
				return ADAPTER_READY;
			} else {
				return ADAPTER_DISABLED;
			}
		} else {
			return ADAPTER_NOT_FOUND;
		}
	}
	
	public static String getHexTagId(final Intent intent) {
        if(intent == null) return null;
        byte[] byte_id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
        if(byte_id == null) return null;

        return getHexString(byte_id);
    }

	// http://stackoverflow.com/questions/22660534/nfc-uid-keyboard-emulator-android
	public static String getDecTagId(final Intent intent, boolean addLeadingZeroes) {
		if(intent == null) return null;
		byte[] byte_id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);

		long result;
		String hexIdString = getHexTagId(intent);

		switch (byte_id.length) {
			// Mifare Classic
			case 4:
				result = Long.parseLong(hexIdString.substring(6, 8) +
						hexIdString.substring(4, 6) +
						hexIdString.substring(2, 4) +
						hexIdString.substring(0, 2), 16);
				break;
			// Mifare Ultralight
			case 7:
			default:
				result = Long.parseLong(hexIdString.substring(4, 6) +
						hexIdString.substring(2, 4) +
						hexIdString.substring(0, 2) +
						"88", 16);
		}

		if (addLeadingZeroes ){
			return String.format("%010d", result);
		} else {
			return Long.toString(result);
		}
	}
	
	// convert bytes to a hex string
    private static String getHexString(final byte[] bytes) {
        StringBuffer hex = new StringBuffer(bytes.length * 2);
        for (int i = 0; i < bytes.length; i++) {
            for (int j = 1; j >= 0; j--) {
                hex.append(HEX[(bytes[i] >> (j * 4)) & 0xF]);
            }
        }
        return hex.toString();
    }
}
