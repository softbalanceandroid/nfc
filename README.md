﻿# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What for ###
* read nfc tags ID

### How to use in Gradle ###

* repositories { maven { url "https://bitbucket.org/softbalanceandroid/nfc/raw/HEAD/maven-repo/" }}
* dependencies { compile 'ru.softbalance:nfc:1.1.0' }

### Fundamental NFC info ###
* [NFC types](https://ru.wikipedia.org/wiki/Mifare)
* [ID discovering according of type - p.10](http://www.nxp.com/documents/application_note/AN10833.pdf)
* [SOF discussion](http://stackoverflow.com/questions/17653589/programming-nfc-tags-using-tagwriter-cant-write-data-on-my-tag)

### EXAMPLES ###
1. "B0:AD:97:C1" -> "C1:97:AD:B0" -> "3247943088" (питерский подорожник ISO/IEC 14443-3 Type A, ID B0:AD:97:C1, ATQA 0х0200, SAK 0х18) MifareClassic 4K
2. "34:39:D0" -> "D0:39:34" + "88"! -> "3493409928" (московская карта метро ISO/IEC 14443-3 Type A, ID 34:39:D0:51:7A:05:66, ATQA 0х4400, SAK 0х00) MifareUltraligth